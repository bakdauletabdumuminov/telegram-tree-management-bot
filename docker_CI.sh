#!/bin/bash

# сборка Docker-образа
docker build -t docker-spring-boot-postgres:latest .

# запуск тестов
docker-compose -f docker-compose.yml up --build -d

# развертывание приложения
docker-compose -f docker-compose.yml up -d